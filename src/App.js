import React, { useState } from 'react';

function App() {
  const [name, setName] = useState('');
  const [language, setLanguage] = useState('');
  const [file, setFile] = useState(null);
  const [greeting, setGreeting] = useState('');

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleLanguageChange = (event) => {
    setLanguage(event.target.value);
  };

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (name && language && file) {
      setGreeting(`Hello, ${name}! Your favorite programming language is ${language}. You uploaded ${file.name} (${file.size} bytes).`);
      setName('');
      setLanguage('');
      setFile(null);
    } else {
      alert('Please enter your name, select your favorite programming language, and upload a file.');
    }
  };

  return (
    <div>
      <h1>Greeting App</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Enter your name:
          <input type="text" value={name} onChange={handleNameChange} />
        </label>
        <label>
          Select your favorite programming language:
          <select value={language} onChange={handleLanguageChange}>
            <option value="">Select...</option>
            <option value="JavaScript">JavaScript</option>
            <option value="Python">Python</option>
            <option value="Java">Java</option>
            <option value="C++">C++</option>
            <option value="Ruby">Ruby</option>
          </select>
        </label>
        <label>
          Upload a file:
          <input type="file" onChange={handleFileChange} />
        </label>
        <button type="submit">Submit</button>
      </form>
      {greeting && <p>{greeting}</p>}
    </div>
  );
}

export default App;
