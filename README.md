# Worldline Assignment - 1

Setting up CI/CD pipeline for web application using GitLab and Jenkins

## GitLab

In GitLab after pushing the React application code to create a CI/CD pipeline, we have to create a `.gitlab-ci.yml` file with the pipeline stages for building and deploying.

In Build Job, `npm i` script is run to download the `node_modules` and create an artifact for it so that the Deploy job can use it to run the application.

In Deploy Job, we run `npm start` to start the application. Node docker image was used for both the stages.

We can see in the [Pipeline](https://gitlab.com/dhruva_02/worldline_assignment_1/-/pipelines) tab that the jobs [Build](https://gitlab.com/dhruva_02/worldline_assignment_1/-/jobs/6196069710) and [Deploy](https://gitlab.com/dhruva_02/worldline_assignment_1/-/jobs/6196069711) ran successfully and gave the expected results.

![alt text](image.png)

## Jenkins

In Jenkins, we connect the project to the GitLab repository to get the code and run it.

Using build steps with Windows Shell, `npm i` and `npm start` were run to implement the project and expected results were given.


![alt text](image-1.png)
